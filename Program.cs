﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static TP3.MovieCollection;

namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Exo1();
            Exo2();
        }


        static void Exo1()
        {
            // Import Collection
            List<WaltDisneyMovies> movies = new MovieCollection().Movies;

            // Count all movies.
            Console.WriteLine("Movies : " + movies.Count());

            //Count all movies with the letter e.
            Console.WriteLine("Movies with charachtere 'e' : " + movies.Count(x => x.Title.Contains("e")));

            int countLetterF = 0;
            foreach (WaltDisneyMovies movie in movies) {
                countLetterF += movie.Title.Count(x => x == 'f');
            }

            //Count how many time the letter f is in all the titles from this list.
            Console.WriteLine("Charachtere f in all titles of the list : " + countLetterF);

            //Display the title of the film with the higher budget.
            movies.Sort(delegate (WaltDisneyMovies x, WaltDisneyMovies y) {
                return y.Budget.CompareTo(x.Budget);
            });
            Console.WriteLine("Title of the film with the higher budget: " + movies[0].Title);

            //Display the title of the movie with the lowest box office.
            movies.Sort(delegate (WaltDisneyMovies x, WaltDisneyMovies y) {
                return x.Budget.CompareTo(y.Budget);
            });
            Console.WriteLine("Title of the film with the lowest budget: " + movies[0].Title);

            //Order the movies by reversed alphabetical order and print the first 11 of the list.
            movies.Sort((x, y) => string.Compare(y.Title, x.Title));
            Console.WriteLine("11 reversed alphabetical movies");
            for (int i = 0; i < 11; i++)
            {
                Console.WriteLine(movies[i].Title);
            }

            // Count all the movies made before 1980
            Console.WriteLine(movies.Count(x => x.ReleaseDate.Year < 1980));

            // Display the average running time of movies having a vowel as the first letter.
            int CountMoviesStartByVowel = 0;
            double countRunningTime = 0;
            foreach (WaltDisneyMovies movie in movies)
            {
                if (movie.Title[0] == 'A' || movie.Title[0] == 'E' || movie.Title[0] == 'I' || movie.Title[0] == 'O' || movie.Title[0] == 'U' || movie.Title[0] == 'Y')
                {
                    CountMoviesStartByVowel++;
                    countRunningTime += movie.RunningTime;
                }
            }
            Console.WriteLine("The average running time of movies having a vowel as the first letter is : " + countRunningTime / CountMoviesStartByVowel);

            //Calculate the mean of all Budget / Box Office of every movie ever
            double countBoxOffice = 0;
            foreach (WaltDisneyMovies movie in movies)
            {
                countBoxOffice += movie.BoxOffice;
            }
            Console.WriteLine("The mean of all Budget : " + countBoxOffice / movies.Count());

            //Print all movies with the letter H or W in the title, but not the letter I or T.
            foreach (WaltDisneyMovies movie in movies)
            {
                if ((movie.Title.Contains('H') || movie.Title.Contains('W') || movie.Title.Contains('h') || movie.Title.Contains('w')))
                {
                    if (!(movie.Title.Contains('I') || movie.Title.Contains('T') || movie.Title.Contains('i') || movie.Title.Contains('t')))
                    {
                        Console.WriteLine(movie.Title);
                    }
                }
            }

        }

        static Mutex mut = new Mutex();
        static void Exo2()
        {
            Thread thread1 = new Thread(new ThreadStart(processus1));
            Thread thread2 = new Thread(new ThreadStart(processus2));
            Thread thread3 = new Thread(new ThreadStart(processus3));
            thread1.Start();
            thread2.Start();
            thread3.Start();
        }

        static void processus1()
        {
            printc("_", 10000, 50);
        }
        static void processus2()
        {
            printc("*", 11000, 40);
        }
        static void processus3()
        {
            printc("°", 9000, 20);
        }

        static void printc(string carachtere, int duration, int interval) {
            mut.WaitOne();
            for (int i = 0; i < duration / interval; i++)
            {
                Console.WriteLine(carachtere);
                Thread.Sleep(interval);
            }
            mut.ReleaseMutex();
        }

    }
}
